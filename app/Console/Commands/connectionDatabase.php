<?php

namespace App\Console\Commands;

use App\Models\Tenant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class connectionDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:migrate {tenant?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations for a specific tenant';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tenantName = $this->argument('tenant');
        if(is_null($tenantName)) {
           $tenantName = $this->ask('which is the tenant could be make migration it');
        }
        $tenant = Tenant::query()->where('name', $tenantName)->first();

        if ($tenant) {
            $this->info("Migrating for tenant: $tenantName");
            config([
                'database.connections.tenant.database' => $tenant->database,
                'database.connections.tenant.username' => $tenant->username,
                'database.connections.tenant.password' => $tenant->password,
            ]);

            Artisan::call('migrate:fresh', [
                '--database' => 'tenant',
                '--force' => true,
            ]);

            $this->info('Migration complete.');
        } else {
            $this->error("Tenant not found: $tenantName");
        }
    }
}
