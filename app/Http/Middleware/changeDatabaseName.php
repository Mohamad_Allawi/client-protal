<?php

namespace App\Http\Middleware;

use App\Models\Tenant;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\InvalidResourceException;

class changeDatabaseName
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $host = $request->host();
        $tenant = Tenant::query()->where('domain',$host)->firstOr(function(){
            throw new InvalidResourceException('bad request',400);
        });

        config()->set('database.connections.tenant.username',$tenant->username);
        config()->set('database.connections.tenant.password',$tenant->password);
        return $next($request);
    }
}
