<?php

use App\Models\Tenant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function (Request $request) {
//     Tenant::create([
//         'name' => 'tenant1' ,
//         'database'  => 'ukrppfmy_client_portal2',
//         'domain' => 'tenant1' ,
//         'username' => 'ukrppfmy_client_protal_user2' ,
//         'password' => 'q;@kyMFH*WHD',
//     ]);

    // DB::statement("CREATE DATABASE tenant1;");
//        return app('currentTenant')->name;
         return User::all();
        // return $request->getHost();
        // return User::all();
    return view('welcome');
})->middleware('changeDatabaseName','tenant');
